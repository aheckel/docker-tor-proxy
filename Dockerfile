FROM registry.gitlab.com/aheckel/docker-s6-base:2.2.0.3-debian-bullseye-slim

ARG DEBIAN_FRONTENT=noninteractive

RUN apt update && \
    apt install --no-install-recommends --yes ca-certificates curl tor && \
    rm --force --recursive /var/lib/apt/lists/*

HEALTHCHECK --interval=60s --timeout=15s --start-period=30s \
            CMD curl --fail --socks5 localhost:9050 'https://check.torproject.org/' || exit 1

USER debian-tor
WORKDIR /var/lib/tor/

EXPOSE 9050

